#!/usr/bin/env python
# coding: utf-8

# In[2]:


import numpy as np
import pandas as pd


# In[3]:


datapath = './homework_data/accidents_2017.csv'   #데이터 불러오기

original_data = pd.read_csv(datapath)
original_data.shape


# In[4]:


original_data.info()
#NULL값은 존재하지 않는다.


# In[5]:


original_data.head(10)  #데이터 확인


# In[6]:


from sklearn import preprocessing

#범주형 변수처리
le_district_name = preprocessing.LabelEncoder()
filtered_data1 = original_data.copy()
filtered_data1['District Name'] = le_district_name.fit_transform(original_data['District Name'])   #nuknown 10

le_neughbirhood_name = preprocessing.LabelEncoder()
filtered_data2 = filtered_data1.copy()
filtered_data2['Neighborhood Name'] = le_neughbirhood_name.fit_transform(filtered_data1['Neighborhood Name'])  #unknown 24

le_street = preprocessing.LabelEncoder()
filtered_data3 = filtered_data2.copy()
filtered_data3['Street'] = le_street.fit_transform(filtered_data2['Street'])

le_month = preprocessing.LabelEncoder()
filtered_data4 = filtered_data3.copy()
filtered_data4['Month'] = le_month.fit_transform(filtered_data3['Month'])

#filtered_data1.head()
le_weekday = preprocessing.LabelEncoder()
filtered_data5 = filtered_data4.copy()
filtered_data5['Weekday'] = le_weekday.fit_transform(filtered_data4['Weekday'])

le_part_of_the_day = preprocessing.LabelEncoder()
final_data = filtered_data5.copy()
final_data['Part of the day'] = le_part_of_the_day.fit_transform(filtered_data5['Part of the day'])
final_data.head(30)


# In[79]:


# 명목변수인 id는 제거 : 단순히 개인을 구분을 위한 변수이므로 모델에 필요하지 않다고 판단
del(final_data['Id'])
final_data.head(30)


# In[8]:


##Feature selection
#1)변수간의 상관성 알아보기
#1-1) 선형 회귀의 가정 중 서로 상관관계를 가지는 변수를 함께 사용하지 않는 것이 좋음
# 상관관계가 높은 변수는 둘 중 하나만 유지하고 나머지는 삭제하거나 두 개 모두 삭제

final_data.corr() 
#Part of the day , Hour -0.542881  
#높은 상관관계는 아니지만 0.5이상이므로 상관관계가 어느정도 있다고 판단하고 두 변수 중 하나를 제거한다. 
#Part of the day가 결국 Hour에 의해 결정되는 값라고 볼 수 있어 상관성이 높은듯 보인다. (두 변수를 모두 사용할 필요가 없다.)
#-> 이후 다른 분석 결과를 통해 제거


# In[9]:


##Feature selection
#1)변수간의 상관성 알아보기
#1-2)예측하고자 하는 변수(target)와 상관관계가 높은 feature값을 찾는다.
#예측변수와 상관성이 높은 변수는 feature에 포함한

cor = final_data.corr()
cor_target = abs (cor["Victims"])
 
relevant_features = cor_target [cor_target> 0.5] 
relevant_features

#0.5이상의 상관계수를 보이는 변수를 추출
#Mild injuries 0.974272
#예측변수인 Victims와 높은 상관성을 보이는 변수는 Mild injuries였고, 모델에 적용시킬 feature에 포함시킨다. 


# In[83]:


##Feature selection
#2) 주성분 분석(PCA) :: 최적의 Feature값을 찾는다.
#분산 비율이 작은 변수를 제거 
from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler


features = ['District Name','Neighborhood Name','Street','Weekday','Month','Day','Hour','Part of the day','Mild injuries','Serious injuries','Vehicles involved','Longitude','Latitude']
x = final_data.loc[:, features].values
y = final_data.loc[:, ['Victims']].values

x = StandardScaler().fit_transform(x)  #정규화

pca = PCA()
fit = pca.fit(x)

pca_fit = pca.fit_transform(x)
pca_df = pd.DataFrame(data = pca_fit)
print("Explained Variance: %s" % fit.explained_variance_ratio_) #투영한 결과의 분산 비율
pca_df.head()

# 분산 비율을 확인해본 결과 각 변수들 중에 높은 비율을 나타내는 값이 없다. (가장 높은 비율이 11% 정도 이므로 이 결과는 선택 기준에 포함하지 않도록 한다.)


# In[87]:


##Feature selection
#3) Feature의 중요도
#랜덤 포레스트 및 엑스트라 트리와 같은 지연된 의사 결정 트리를 사용하여 기능의 중요성을 평가할 수 있습니다.
#Feature Importance with Extra Trees Classifier

from sklearn.ensemble import ExtraTreesClassifier

features = ['District Name','Neighborhood Name','Street','Weekday','Month','Day','Hour','Part of the day','Mild injuries','Serious injuries','Vehicles involved','Victims','Longitude','Latitude']

array = final_data.loc[:, features].values

X = array[:,0:14]
Y = array[:,11]  #Victims

# feature extraction
model = ExtraTreesClassifier(n_estimators=10)
model.fit(X, Y)
print(model.feature_importances_)

#의사결정트리를 활용하여 평가된 변수의 중요성을 살펴봤을 때, 중요도가 높게 나온 변수를 확인한다.
#당연한 결과로 Victims가 예측변수이므로 중요도 51.3%로 가장 높게 나왔고 
#'Mild injuries'가 중요도 43.1%로 가장 높게 나왔고,두번째로 중요도가 높게 나왔다. 
#상대적으로 비교했을 때 가장 중요도가 아주 높은 변수라고 할 수 있다. 


# In[95]:


##Feature selection
#4) 일 변량 선택
#f_regression을 사용하여 SelectKBest
#변수와 밀접한 관계가있는 기능을 선택할 수 있다.

from sklearn.feature_selection import SelectKBest
from sklearn.feature_selection import f_regression
from numpy import set_printoptions

features = ['Weekday','Month','Day','Hour','Mild injuries','Serious injuries','Vehicles involved','Longitude','Latitude']

x = final_data.loc[:,features].values
y = final_data.loc[:,['Victims']].values

test = SelectKBest(score_func=f_regression,k=3)  #상위 3개를 뽑아 보면
fit = test.fit(x,y)

set_printoptions(precision=3)
#print(fit.scores_)
features = fit.transform(x)
print(features[0:20,:])  #선택된 데이터 20번째 행까지 보기
# 데이터프레임과 비교했을 때 각각 순서대로 
# Mild injuries, Serious injuries,Vehicles involved가 best select로 추출됨
# 세 변수를 모델에 적용할 Feature에 포함 


# In[ ]:


#지금까지 Feature selection 결과를 요약해서 Mild injuries, Serious injuries,Vehicles involved을 Feature로 결정하고 모델을 돌려보도록 함


# In[14]:


##선형,릿지,라쏘 train/test data

from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression, Ridge, Lasso #, LogisticRegression

features = ['Mild injuries','Serious injuries','Vehicles involved']   #feature 선정

train_d, test_d = train_test_split(final_data,test_size=0.1)   
#train/test 데이터 나누기
#test 데이터를 원 데이터의 10% 사용

train_y = train_d['Victims']   #Victims를 예측한다. 
train_x = train_d[features]

test_y = test_d['Victims']
test_x = test_d[features]

models = [LinearRegression(),Ridge(),Lasso(alpha=0.001)]  #,LogisticRegression(solver='lbfgs')
for m in models:
    m.fit(train_x,train_y)  #학습
    print('model {} = {}'.format(m,m.score(test_x,test_y)))   #평가


# In[15]:


#선택된 Feature를 모델에 적용
#로지스틱 회귀모형의 경우, 분류에 적합한 모델이므로 제외하고 선형, 릿지, 라쏘 회귀모형에 적용하여 MAE값을 비교해서 모델을 선택한다.


# In[98]:


##선형 k-fold 

from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
from sklearn.metrics import (r2_score, mean_absolute_error,mean_squared_error)


features = ['Mild injuries','Serious injuries','Vehicles involved']

kf = KFold(n_splits=10,shuffle=True)

accrs = []
fold_idx = 1
for train_idx, test_idx in kf.split(final_data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d = final_data.iloc[train_idx], final_data.iloc[test_idx]
    
    train_y = train_d['Victims']
    train_x = train_d[features]
    
    test_y = test_d['Victims']
    test_x = test_d[features]
    
    model = LinearRegression()
    fit = model.fit(train_x,train_y)
    
    mean_accr = model.score(test_x,test_y)
    
    predict_y = fit.predict(test_x)
    
    accrs.append(mean_accr)
    print(mean_absolute_error(test_y, predict_y))
    fold_idx +=1
      
print('MAE: ', mean_absolute_error(test_y, predict_y))
#print('Accuracy_average : ',np.average(accrs))


# In[100]:


##릿지 k-fold 

from sklearn.model_selection import KFold
from sklearn.linear_model import Ridge
from sklearn.metrics import (r2_score, mean_absolute_error,mean_squared_error)


features = ['Mild injuries','Serious injuries','Vehicles involved']

kf = KFold(n_splits=10,shuffle=True)

accrs = []
fold_idx = 1
for train_idx, test_idx in kf.split(final_data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d = final_data.iloc[train_idx], final_data.iloc[test_idx]
    
    train_y = train_d['Victims']
    train_x = train_d[features]
    
    test_y = test_d['Victims']
    test_x = test_d[features]
    
    model = Ridge()
    fit = model.fit(train_x,train_y)
    
    mean_accr = model.score(test_x,test_y)
    
    predict_y = fit.predict(test_x)
    
    accrs.append(mean_accr)
    print(mean_absolute_error(test_y, predict_y))
    #print(mean_accr)
    
    fold_idx +=1

print('MAE: ', mean_absolute_error(test_y, predict_y))
#print('Accuracy_average : ',np.average(accrs))


# In[101]:


##라쏘 k-fold 

from sklearn.model_selection import KFold
from sklearn.linear_model import Lasso
from sklearn.metrics import (r2_score, mean_absolute_error,mean_squared_error)


features = ['Mild injuries','Serious injuries','Vehicles involved']

kf = KFold(n_splits=10,shuffle=True)

accrs = []
fold_idx = 1
for train_idx, test_idx in kf.split(final_data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d = final_data.iloc[train_idx], final_data.iloc[test_idx]
    
    train_y = train_d['Victims']
    train_x = train_d[features]
    
    test_y = test_d['Victims']
    test_x = test_d[features]
    
    model = Lasso(alpha =0.01)
    fit = model.fit(train_x,train_y)
    
    mean_accr = model.score(test_x,test_y)
    
    predict_y = fit.predict(test_x)
    
    accrs.append(mean_accr)
    print(mean_absolute_error(test_y, predict_y))
    fold_idx +=1

print ('MAE: ', mean_absolute_error(test_y, predict_y))
#print('Accuracy_average : ',np.average(accrs))


# In[ ]:


#선형 MAE:  0.0025486418379286864
#릿지 MAE:  0.002709936753768353
#라쏘 MAE:  0.028627415314770244

#MAE 값을 비교했을 때 라쏘 회귀모형이 가장 큰 MAE값을 보이므로 이 모델은 제외
#선형과 릿지는 약 0.002로 작은 MSE값을 보인다. 


# In[ ]:


#모델 파라미터 엔지니어링
#모델의 성능이 다 높게 나오므로 데이터 분포를 확인
#각 변수가 데이터가 불균형하므로 데이터의 분포를 가장 많은 데이터의 개수로 맞춰 over sampling을 시도


# In[155]:


#변수 1 ::Mild injuries
#데이터 분포 확인
d2 = final_data['Mild injuries'].value_counts()
pd.DataFrame(d2)

d2.plot(kind='bar')


# In[156]:


#over-sampling
#데이터 불균형을 보완

d2 = final_data['Mild injuries'].value_counts() #편향
pd.DataFrame(d2)

count_class_1, count_class_2, count_class_0, count_class_3, count_class_4, count_class_5, count_class_6, count_class_7, count_class_10, count_class_9, count_class_8 = final_data['Mild injuries'].value_counts()
# 값에 따라 카운트 된 값을 새로운 변수에 저장
print(count_class_1)
print(count_class_2)
print(count_class_0)
print(count_class_3)
print(count_class_4)
print(count_class_5)
print(count_class_6)
print(count_class_7)
print(count_class_10)
print(count_class_9)
print(count_class_8)
# Divide by class

df_class_1 = final_data[final_data['Mild injuries'] == 1]
df_class_2 = final_data[final_data['Mild injuries'] == 2]
df_class_0 = final_data[final_data['Mild injuries'] == 0]
df_class_3 = final_data[final_data['Mild injuries'] == 3]
df_class_4 = final_data[final_data['Mild injuries'] == 4]
df_class_5 = final_data[final_data['Mild injuries'] == 5]
df_class_6 = final_data[final_data['Mild injuries'] == 6]
df_class_7 = final_data[final_data['Mild injuries'] == 7]
df_class_10 = final_data[final_data['Mild injuries'] == 10]
df_class_9 = final_data[final_data['Mild injuries'] == 9]
df_class_8 = final_data[final_data['Mild injuries'] == 8]


# In[157]:


df_class_2_over = df_class_2.sample(count_class_1, replace=True)
df_class_0_over = df_class_0.sample(count_class_1, replace=True) 
df_class_3_over = df_class_3.sample(count_class_1, replace=True) 
df_class_4_over = df_class_4.sample(count_class_1, replace=True)
df_class_5_over = df_class_5.sample(count_class_1, replace=True)
df_class_6_over = df_class_6.sample(count_class_1, replace=True)
df_class_7_over = df_class_7.sample(count_class_1, replace=True)
df_class_10_over = df_class_10.sample(count_class_1, replace=True)
df_class_9_over = df_class_9.sample(count_class_1, replace=True)
df_class_8_over = df_class_8.sample(count_class_1, replace=True)



df_test_over1 = pd.concat([df_class_1, df_class_2_over, df_class_0_over, df_class_3_over, df_class_4_over, df_class_5_over,df_class_6_over,df_class_7_over,df_class_10_over,df_class_9_over,df_class_8_over],axis=0)

print('Random over-sampling:')
print(df_test_over1['Mild injuries'].value_counts())

df_test_over1['Mild injuries'].value_counts().plot(kind='bar', title='Count (target)');

new_Mild_injuries = df_test_over1.loc[:,['Mild injuries']]


# In[159]:


#변수 2 ::Vehicles involved
#데이터 분포 확인
d1 = final_data['Vehicles involved'].value_counts()
pd.DataFrame(d1)
d1.plot(kind='bar')


# In[160]:


#over-sampling
#데이터 불균형을 보완

d2 = final_data['Vehicles involved'].value_counts() #편향
pd.DataFrame(d2)

count2_class_2, count2_class_1, count2_class_3, count2_class_4, count2_class_5, count2_class_6, count2_class_7, count2_class_9, count2_class_0, count2_class_10, count2_class_8 = final_data['Mild injuries'].value_counts()
# 값에 따라 카운트 된 값을 새로운 변수에 저장
print(count2_class_2)
print(count2_class_1)
print(count2_class_3)
print(count2_class_4)
print(count2_class_5)
print(count2_class_6)
print(count2_class_7)
print(count2_class_9)
print(count2_class_0)
print(count2_class_10)
print(count2_class_8)
#print(count2_class_14)
#print(count2_class_13)
#print(count2_class_11)

df2_class_2 = final_data[final_data['Vehicles involved'] == 2]
df2_class_1 = final_data[final_data['Vehicles involved'] == 1]
df2_class_3 = final_data[final_data['Vehicles involved'] == 3]
df2_class_4 = final_data[final_data['Vehicles involved'] == 4]
df2_class_5 = final_data[final_data['Vehicles involved'] == 5]
df2_class_6 = final_data[final_data['Vehicles involved'] == 6]
df2_class_7 = final_data[final_data['Vehicles involved'] == 7]
df2_class_9 = final_data[final_data['Vehicles involved'] == 9]
df2_class_0 = final_data[final_data['Vehicles involved'] == 0]
df2_class_10 = final_data[final_data['Vehicles involved'] == 10]
df2_class_8 = final_data[final_data['Vehicles involved'] == 8]
#df2_class_14 = final_data[final_data['Vehicles involved'] == 14]
#df2_class_13 = final_data[final_data['Vehicles involved'] == 13]
#df2_class_11 = final_data[final_data['Vehicles involved'] == 11]
# 11,13,14의 경우 값이 충분치 않다는 에러 발생으로 제거 하고 진행


# In[161]:


df2_class_2_over = df2_class_2.sample(count_class_1, replace=True)
df2_class_1_over = df2_class_1.sample(count_class_1, replace=True) 
df2_class_3_over = df2_class_3.sample(count_class_1, replace=True) 
df2_class_4_over = df2_class_4.sample(count_class_1, replace=True)
df2_class_5_over = df2_class_5.sample(count_class_1, replace=True)
df2_class_6_over = df2_class_6.sample(count_class_1, replace=True)
df2_class_7_over = df2_class_7.sample(count_class_1, replace=True)
df2_class_9_over = df2_class_9.sample(count_class_1, replace=True)
df2_class_0_over = df2_class_0.sample(count_class_1, replace=True) 
df2_class_10_over = df2_class_10.sample(count_class_1, replace=True)
df2_class_8_over = df2_class_8.sample(count_class_1, replace=True)



df_test_over2 = pd.concat([df2_class_2_over, df2_class_1_over, df2_class_3_over, df2_class_4_over, df2_class_5_over,df2_class_6_over,df2_class_7_over,df2_class_9_over,df2_class_0_over,df2_class_10_over,df2_class_8_over],axis=0)

print('Random over-sampling:')
print(df_test_over2['Vehicles involved'].value_counts())

df_test_over2['Vehicles involved'].value_counts().plot(kind='bar', title='Count (target)');

new_Vehicles_involved = df_test_over2.loc[:,['Vehicles involved']]


# In[168]:


#변수 3 ::Serious injuries
#데이터 분포 확인
d3 = final_data['Serious injuries'].value_counts() #편향
pd.DataFrame(d3)
d3.plot(kind='bar')


# In[163]:


#over-sampling
#데이터 불균형을 보완

d3 = final_data['Serious injuries'].value_counts() #편향
pd.DataFrame(d3)

count1_class_0, count1_class_1, count1_class_2, count1_class_4 = final_data['Serious injuries'].value_counts()
# 값에 따라 카운트 된 값을 새로운 변수에 저장
print(count1_class_0)
print(count1_class_1)
print(count1_class_2)
print(count1_class_4)
# Divide by class
df1_class_0 = final_data[final_data['Serious injuries'] == 0]
df1_class_1 = final_data[final_data['Serious injuries'] == 1]
df1_class_2 = final_data[final_data['Serious injuries'] == 2]
df1_class_4 = final_data[final_data['Serious injuries'] == 4]


# In[164]:


df1_class_0_over = df1_class_0.sample(count_class_1, replace=True)
df1_class_1_over = df1_class_1.sample(count_class_1, replace=True) 
#0 : 가장 많은 데이터 수에 맞게 1번 데이터를 샘플링 (1번 데이터가 있는 프레임에서 추출해서 1번의 빈도를 높여줌)
#replace=True 복원추출
df1_class_2_over = df1_class_2.sample(count_class_1, replace=True) 
df1_class_4_over = df1_class_4.sample(count_class_1, replace=True) 



df_test_over3 = pd.concat([df1_class_0_over, df1_class_1_over, df1_class_2_over, df1_class_4_over], axis=0)

print('Random over-sampling:')
print(df_test_over3['Serious injuries'].value_counts())

df_test_over3['Serious injuries'].value_counts().plot(kind='bar', title='Count (target)');

df_test_over3.head()
new_Serious_injuries = df_test_over3.loc[:,['Serious injuries']]


# In[167]:


app = new_Mild_injuries.append(new_Vehicles_involved)
over_data = app.append(new_Serious_injuries)

over_data.shape #79783


# In[169]:


re_data = final_data.loc[:,['Mild injuries','Vehicles involved','Serious injuries']]
re_data.head()


#이 데이터로 사용 피쳐 선택 후
#피처 중 가장 많은 row 개수에 맞게 샘플링?


# In[ ]:


##선형 k-fold 

from sklearn.model_selection import KFold
from sklearn.linear_model import LinearRegression
from sklearn.metrics import (r2_score, mean_absolute_error,mean_squared_error)


features = ['Mild injuries','Serious injuries','Vehicles involved']

kf = KFold(n_splits=10,shuffle=True)

accrs = []
fold_idx = 1
for train_idx, test_idx in kf.split(over_data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d = final_data.iloc[train_idx], final_data.iloc[test_idx]
    
    train_y = train_d['Victims']
    train_x = train_d[features]
    
    test_y = test_d['Victims']
    test_x = test_d[features]
    
    model = LinearRegression()
    fit = model.fit(train_x,train_y)
    
    mean_accr = model.score(test_x,test_y)
    
    predict_y = fit.predict(test_x)
    
    accrs.append(mean_accr)
    print(mean_absolute_error(test_y, predict_y))
    #print(mean_accr)
    fold_idx +=1
      
print('MAE: ', mean_absolute_error(test_y, predict_y))
#print('Accuracy_average : ',np.average(accrs))


# In[ ]:


##릿지 k-fold 

from sklearn.model_selection import KFold
from sklearn.linear_model import Ridge
from sklearn.metrics import (r2_score, mean_absolute_error,mean_squared_error)


features = ['Mild injuries','Serious injuries','Vehicles involved']

kf = KFold(n_splits=10,shuffle=True)

accrs = []
fold_idx = 1
for train_idx, test_idx in kf.split(over_data):
    print('Fold{}'.format(fold_idx))
    train_d,test_d = final_data.iloc[train_idx], final_data.iloc[test_idx]
    
    train_y = train_d['Victims']
    train_x = train_d[features]
    
    test_y = test_d['Victims']
    test_x = test_d[features]
    
    model = Ridge()
    fit = model.fit(train_x,train_y)
    
    mean_accr = model.score(test_x,test_y)
    
    predict_y = fit.predict(test_x)
    
    accrs.append(mean_accr)
    print(mean_absolute_error(test_y, predict_y))
    #print(mean_accr)
    
    fold_idx +=1

print('MAE: ', mean_absolute_error(test_y, predict_y))
#print('Accuracy_average : ',np.average(accrs))


# In[ ]:


#최종 모델 설정


# In[ ]:


#(0) 문제 (1 점) : 3 ~ 4 줄
#-입력, 출력의 정의
#-분류입니까? 회귀?  
 # 회귀 모델을 사용한다. 
#-라벨 수는 몇 개입니까?
#-데이터는 어떻게 생겼습니까?  
 # 원 데이터의 형태는 10339개의 데이터이고, 15개의 컬럼을 가지는 데이터
 # 데이터 타입은 float64(2), int64(6), object(7)이다. 

#(1) 특징 (1 점) : 3 ~ 4 라인
#-직접 기능을 선택하거나 정의 할 수 있습니다
#-데이터를 어떻게 전처리 했습니까?  
 #info 함수를 통해 데이터의 타입과 변수와 데이터의 갯수, null값 존재 여부 등 을 한꺼번에 확인한다. null값은 존재하지 않으므로 결측치 
 #처리는 생략하였고, regression에 적용하기 위해 문자형으로 된 데이터를 수치화함.
 #id에 대한 컬럼은 분석에 필요없다고 판단하여 제거함
 #바르셀로나시의 지방 경찰이 처리 한 사고 목록. 부상 수, 심각도, 차량 수 및 영향 지점별로 부상 수를 통합한 데이터

#(2) 모델 (2 점) : 3 ~ 4 라인
#-다음 목록 중 하나를 사용해야합니다
#-왜 모델을 선택 했습니까? 
#-모형리스트 : 선형 회귀, 릿지 회귀, 올가미 회귀, 로지스틱 회귀
#선형 회귀 -> 최소의 MSE값을 찾아줌 
#릿지 회귀 -> 평균 오차 제곱(MSE)를 최소화하면서 벡터의 L2 norm을 최소화하는 기법 (정규화 선형회귀)
#라소 회귀 -> 릿지 회귀와 동일하지만 L1 norm을 제약한다는 점이 다르다. (정규화 선형회귀)
#로지스틱 회귀 -> 종속변수가 이산형 일때 사용 로지스틱의 경우 분류를 목적으로 하기 때문에 적합하지 않다고 판단
#종속변수가 이산형 일때, 로지스틱의 경우 분류를 목적으로 하기 때문에 적합하지 않다고 판단하여 로지스틱 회귀분석은 모델에서 제외시켰다.
#실제로 정확도도 가장 낮게 나왔음.
#나머지 선형,릿지,라쏘 회귀모델들을 사용해서  


#(3) 측정 (2 점) : 3 ~ 4 줄
#-측정 단계 설명
#-10 배 교차 검증
#-분류 : 정확도
#-회귀 : MAE (평균 절대 오차)
#-주의 : 두 개 이상의 데이터 파일이 있으면 모든 파일을 사용해야합니다
#-(예) 10 배 교차 검증을위한 {train.data, test.data}-> {all.data}

#(4) 모델 파라미터 엔지니어링 (4 점)
#-성능 향상을 위해 매개 변수를 어떻게 변경 했습니까?
#-이유를 설명하시오
#-(예) 'C'값을 100으로 변경했기 때문에 ....
#-데이터 불균형, 데이터 희소성 확인 ...
#-이걸 어떻게 다루는 지 설명해
#-고성능이 항상 좋은 것은 아닙니다. 예를 들어, 데이터 불균형으로 인해 90 ~ 95 %가 될 수 있습니다. 이 문제를 어떻게 처리 할 수 있습니까?

